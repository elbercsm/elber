#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->hellolabel->setVisible(false);
    ui->actionreset->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_HellopushButton_clicked()
{
    ui->hellolabel->setVisible(true);
    ui->actionreset->setEnabled(true);
    ui->HellopushButton->setStyleSheet("QPushButton {background-color:rgb(123,124,125);}");
    ui->hellolabel->setText("Hello Elber");
}


void MainWindow::on_actionreset_triggered()
{
    ui->actionreset->setEnabled(false);
    ui->hellolabel->setVisible(false);
    ui->HellopushButton->setStyleSheet("QPushButton {background-color:white;}");
}
