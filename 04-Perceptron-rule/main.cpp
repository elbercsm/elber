#include "mainwindow.h"
#include <QApplication>
#include "perceptron.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    //w.show();

    QVector<int> inputs;
    QMap<QVector<int>,int> trainingSet;
    //AND port inputs: bipolar input and target(-1,1)
    inputs.append(1);
    inputs.append(1);
    trainingSet.insert(inputs, 1);

    inputs[0] = 1;
    inputs[1] = -1;
    trainingSet.insert(inputs, -1);

    inputs[0] = -1;
    inputs[1] = 1;
    trainingSet.insert(inputs, -1);

    inputs[0] = -1;
    inputs[1] = -1;
    trainingSet.insert(inputs, -1);

    qDebug()<< "Inputs\t\tTarget";
    QMap<QVector<int>,int>::iterator it;
    for(it = trainingSet.begin(); it != trainingSet.end(); it++){
        QString inputs = "";
        for(int i = 0; i < it.key().size(); i++){
            inputs += QString::number(it.key().at(i))+ " ";
        }
        qDebug()<<inputs.remove(inputs.size() - 1, 1) << "\t\t" << it.value();
    }

    //Perceptron Rule
    Perceptron *myPerceptron = new Perceptron();
    myPerceptron->UpdateParameters(trainingSet, 0, 0, 1);
    QVector<double> weights = myPerceptron->CalculateWeights();
    int epochs = myPerceptron->GetNumOfEpochs();

    qDebug()<<"\n***Perceptron Rule***\n\tWeights: ";

    for(int i = 0; i < weights.size(); i++){

    qDebug()<< "\t\tw" << (i+1) << " = " << weights.at(i) << "\n";

    }

    qDebug()<< "\tEpochs: "<< epochs;






    return a.exec();
}
