#ifndef PERCEPTRON_H
#define PERCEPTRON_H

#include <QVector>
#include <QMap>

class Perceptron
{
public:
    Perceptron();

    int GetNumOfEpochs(void);
    void UpdateParameters(QMap<QVector<int>,int> trainingSet,
                          int bias,
                          double tetha,
                          double alpha);
    void InitWeights(void);
    double CalculateNet(QVector<int> inputs);
    int CalculateOutput(QVector<int> inputs);
    bool CheckStopCondition(QVector<double> oldWeights, int oldBias);
    QVector<double> CalculateWeights(void);


private:
    int m_bias;
    int m_numOfInputs;
    int m_numOfEpochs;
    double m_tetha;
    double m_alpha;

    QVector<double> m_weights;
    QMap<QVector<int>,int> m_trainingSet;

};

#endif // PERCEPTRON_H
