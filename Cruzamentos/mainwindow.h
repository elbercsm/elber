#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QRandomGenerator>
#include <QMessageBox>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

private:
    QString pai1;
    QString pai2;
    QString primeiraSelecao;
    QString segundaSelecao;
    QString filho1;
    QString filho2;
    int corte1;
    QMessageBox msgBox;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
