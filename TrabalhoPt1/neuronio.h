#ifndef NEURONIO_H
#define NEURONIO_H
#include <QVector>

class neuronio
{
public:
    neuronio();
    double CalculaRede(QVector<int> entrada, QVector<double> pesos, int bias);
    double CalculaSaida(QVector<int> entada, int tetha, QVector<double> pesos, int bias);

};

#endif // NEURONIO_H
