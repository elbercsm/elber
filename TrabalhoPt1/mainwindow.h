#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include<QDebug>
#include <QVector>
#include <QMap>
#include "perceptron.h"
#include "hebb.h"
#include "neuronio.h"


#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_pushButtonVai_clicked();

private:
    Ui::MainWindow *ui;

    bool hebbAprendeu, percepAprendeu;
    int tethaP, biasP,tethaH, biasH,epocas;
    QVector<double> pesoP, pesoH;
    QMap<QVector<int>,int> MapaTreino;
    QVector<int> entradas;

};

#endif // MAINWINDOW_H
