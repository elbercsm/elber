#ifndef HEBB_H
#define HEBB_H

#include <QVector>
#include <QMap>
#include <QDebug>

class Hebb{

public:
    Hebb();
    int GetBias();
    QVector<double> CalculateWeights(QMap<QVector<int>, int> trainingSet);
private:
    int m_bias;

};


#endif // HEBB_H
