#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QMap<QVector<int>,int> transformaInput(QString in, QString out){
    QStringList inputLine = in.split("\n");
    QMap<QVector<int>,int> trainingSet;
    QStringList outputLine = out.split("\n");
    for (int i = 0; i < inputLine.size()-1; i++) {
        QVector<int> auxIn = QVector<int>();
        for (int j = 0; j < inputLine.at(i).split(" ").size(); j++) {
            auxIn.append(inputLine.at(i).split(" ").at(j).toInt());
        }
        //  qDebug()<<"IN "<<auxIn<<"OUT "<<outputLine.at(i).toInt();
        trainingSet.insert(auxIn,outputLine.at(i).toInt());
    }
    return trainingSet;
}

void MainWindow::on_pushButtonVai_clicked()
{
    //treinamento
    if(ui->comboBoxAcao->currentIndex()==0){
        MapaTreino =   transformaInput(ui->textBrowserEntrada->toPlainText(),ui->textBrowserSaida->toPlainText());
        //hebb
        if(ui->comboBoxSelecao->currentIndex()==0){
            Hebb *meuHebb = new Hebb;
            pesoH = meuHebb->CalculateWeights(MapaTreino);
            biasH = meuHebb->GetBias();
            tethaH = 0;
            ui->labelPeso1->setText(QString("Peso1: %1").arg(pesoH.at(0)));
            ui->labelPeso2->setText(QString("Peso2: %1").arg(pesoH.at(1)));
            ui->labelBias->setText(QString("Bias: %1").arg(biasH));
            ui->labelEpoca->setText("");
            hebbAprendeu = true;
            ui->statusBar->showMessage("Hebb foi treinado!");
            //perceptron
        }else  if(ui->comboBoxSelecao->currentIndex()==1){
            Perceptron *meuPerceptron = new Perceptron;
            meuPerceptron->UpdateParameters(MapaTreino, 0,0,1);
            pesoP = meuPerceptron->CalculateWeights();
            epocas= meuPerceptron->GetNumOfEpochs();
            biasP = meuPerceptron->GetBias();
            tethaP = meuPerceptron->GetTetha();
            ui->labelPeso1->setText(QString("Peso1: %1").arg(pesoP.at(0)));
            ui->labelPeso2->setText(QString("Peso2: %1").arg(pesoP.at(1)));
            ui->labelBias->setText(QString("Bias: %1").arg(biasP));
            ui->labelEpoca->setText(QString("Epocas: %1").arg(epocas));
            percepAprendeu = true;
            ui->statusBar->showMessage("Perceptron foi treinado!");
        }

    }else if (ui->comboBoxAcao->currentIndex() == 1){
        entradas = {ui->lineEdit->text().toInt(), ui->lineEdit_2->text().toInt()};
        neuronio *meuNeuronio = new neuronio;
        if(ui->comboBoxSelecao->currentIndex()==0){
            if(!hebbAprendeu){
                ui->statusBar->showMessage("Hebb ainda nao foi treinado !");
            }else{
                ui->statusBar->showMessage(QString("Saida Hebb: %1").arg(meuNeuronio->CalculaSaida(entradas,tethaH,pesoH,biasH)));
            }
        }else if(ui->comboBoxSelecao->currentIndex()==1){
            if(!percepAprendeu){
                ui->statusBar->showMessage("Perceptron ainda nao foi treinado !");
            }else{
                ui->statusBar->showMessage(QString("Saida Perceptron: %1").arg(meuNeuronio->CalculaSaida(entradas,tethaP,pesoP,biasP)));
            }
        }

    }
}

