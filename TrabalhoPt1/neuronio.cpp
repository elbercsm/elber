#include "neuronio.h"

neuronio::neuronio()
{
}


double neuronio::CalculaRede(QVector<int> entrada, QVector<double> pesos, int bias){
    int rede = 0;
    for(int i = 0; i < entrada.size(); i++){
        rede += entrada.at(i) * pesos.at(i);
    }
    rede += bias;
    return rede;
}


double neuronio :: CalculaSaida(QVector<int> entrada, int tetha, QVector<double> pesos, int bias){

    int rede = CalculaRede(entrada, pesos, bias);

    if (rede > tetha){
        return 1;
    }else if(rede < (-1)*tetha){
        return -1;
    }else{
        return 0;
    }
}
