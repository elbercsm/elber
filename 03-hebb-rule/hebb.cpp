#include "hebb.h"

Hebb::Hebb(){

}

QVector<double> Hebb::CalculateWeights(QMap<QVector<int>,int> trainingSet){
    QVector<double> weights;

    //Number of inputs
    int numOfInputs = trainingSet.begin().key().size();

    //Inintializing weight vector
    for (int i = 0; i < numOfInputs; i++){
        weights.append(0);
    }

    //calculate weights
    QMap<QVector<int>, int>::iterator it;

        for(it = trainingSet.begin(); it != trainingSet.end(); it++){
        //The map is a table. This table presents the inputs as the key
        //and the target as the value.
        QVector<int> inptus = it.key();
        int target = it.value();

            for(int i = 0; i < numOfInputs; i++){
            //Canvas item 8.3.1
            weights[i] = weights.at(i) + inptus.at(i)*target;
            }

        }
    return weights;
}
