#ifndef HEBB_H
#define HEBB_H

#include <QVector>
#include <QMap>
#include <QDebug>

class Hebb{
public:
    Hebb();

    QVector<double> CalculateWeights(QMap<QVector<int>, int> trainingSet);
};

#endif // HEBB_H
