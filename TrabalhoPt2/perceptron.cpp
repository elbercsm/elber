#include "perceptron.h"
#include <QDebug>

Perceptron::Perceptron()
    {
        m_bias = 0;
        m_tetha = 0;
        m_alpha = 1;
        m_numOfInputs = 0;
        m_numOfEpochs = 1;

        m_trainingSet =  QVector<QPair<QVector<int>, int>>();
        m_weights = QVector<double>();

    }



int Perceptron::GetNumOfEpochs()
    {
        return m_numOfEpochs;
    }


int Perceptron::GetBias(){
        return m_bias;
    }


int Perceptron::GetTetha(){
        return m_tetha;
    }

void Perceptron::UpdateParameters( QVector<QPair<QVector<int>, int>> trainingSet, int bias, double tetha, double alpha)
    {
        m_trainingSet = trainingSet;
        m_bias = bias;
        m_tetha = tetha;
        m_alpha = alpha;

        m_numOfInputs = m_trainingSet.at(0).first.size();

        InitWeights();
    }

void Perceptron::InitWeights()
    {
        for (int i = 0; i < m_numOfInputs; i++)
        {
            m_weights.append(0);
        }
    }

double Perceptron::CalculateRede(QVector<int> inputs)
    {
        int net = 0;

        for(int i = 0; i < inputs.size(); i++)
        {
            net += inputs.at(i) * m_weights.at(i);
        }

        net += m_bias;

        return net;
    }

int Perceptron::CalculateOutput(QVector<int> inputs)
    {
        double net = CalculateRede(inputs);

        if (net > m_tetha)
        {
            return 1;
        }
        else if (net < (-1)*m_tetha)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }

bool Perceptron::CheckStopCondition(QVector<double> oldWeights, double oldBias)
    {

        for(int i = 0; i < m_weights.size(); i++)
        {
            if (m_weights.at(i) != oldWeights.at(i))
            {
                return false;
            }
        }

        if (m_bias != oldBias)
        {
            return false;
        }

        return true;
    }

QVector<double> Perceptron::CalculateWeights()
    {
        //Initialize old weights and bias
        QVector<double> oldWeights = m_weights;
        double oldBias = m_bias;


        for(int i = 0; i <  m_trainingSet.size(); i++){

            QVector<int> inputs = m_trainingSet.at(i).first;
            int target =  m_trainingSet.at(i).second;
            int output = CalculateOutput(inputs);

            //If output != target, update weights
            if (output != target)
            {
                for (int i = 0; i < m_numOfInputs; i++)
                {
                    m_weights[i] = m_weights.at(i) + m_alpha*inputs.at(i)*target;
                }
                m_bias = m_bias + m_alpha*target;
            }
        }

        if (CheckStopCondition(oldWeights, oldBias))
        {
            return m_weights;
        }
        else
        {
            m_numOfEpochs++;
            return CalculateWeights();
        }

    }
