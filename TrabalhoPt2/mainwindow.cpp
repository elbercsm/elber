#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_black = QIcon(":/pixels/black.png");
    m_white = QIcon(":/pixels/white.png");

    //trainingSet for each neuron on neural net
    m_trainingSet = QVector<QVector<QPair<QVector<int>,int>>>();

    // Input vector
    m_inputs = QVector<int>();
    for(int i = 0; i < 80; i++)
        m_inputs.append(-1);

    // Construct a vector with PushButtons (pixels)
    PopulatePixelsVector();

    // Construct the numbers
    PopulateTrainingSet();



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_characterComboBox_currentIndexChanged(int index)
{
    if (index == 0)
    {
        QVector<int> aux;
        for(int i = 0; i < 80; i++)
            aux.append(-1);

        UpdateDisplay(aux);
        m_inputs = aux;
    }
    else
    {
        UpdateDisplay(m_fontA.at(index - 1));
        m_inputs = m_fontA.at(index - 1);
    }
}


void MainWindow::PopulatePixelsVector(void)
{
    m_pixels = QVector<QPushButton*>();
    m_pixels.append(ui->pixel11);
    m_pixels.append(ui->pixel12);
    m_pixels.append(ui->pixel13);
    m_pixels.append(ui->pixel14);
    m_pixels.append(ui->pixel15);
    m_pixels.append(ui->pixel16);
    m_pixels.append(ui->pixel17);
    m_pixels.append(ui->pixel18);

    m_pixels.append(ui->pixel21);
    m_pixels.append(ui->pixel22);
    m_pixels.append(ui->pixel23);
    m_pixels.append(ui->pixel24);
    m_pixels.append(ui->pixel25);
    m_pixels.append(ui->pixel26);
    m_pixels.append(ui->pixel27);
    m_pixels.append(ui->pixel28);

    m_pixels.append(ui->pixel31);
    m_pixels.append(ui->pixel32);
    m_pixels.append(ui->pixel33);
    m_pixels.append(ui->pixel34);
    m_pixels.append(ui->pixel35);
    m_pixels.append(ui->pixel36);
    m_pixels.append(ui->pixel37);
    m_pixels.append(ui->pixel38);

    m_pixels.append(ui->pixel41);
    m_pixels.append(ui->pixel42);
    m_pixels.append(ui->pixel43);
    m_pixels.append(ui->pixel44);
    m_pixels.append(ui->pixel45);
    m_pixels.append(ui->pixel46);
    m_pixels.append(ui->pixel47);
    m_pixels.append(ui->pixel48);

    m_pixels.append(ui->pixel51);
    m_pixels.append(ui->pixel52);
    m_pixels.append(ui->pixel53);
    m_pixels.append(ui->pixel54);
    m_pixels.append(ui->pixel55);
    m_pixels.append(ui->pixel56);
    m_pixels.append(ui->pixel57);
    m_pixels.append(ui->pixel58);

    m_pixels.append(ui->pixel61);
    m_pixels.append(ui->pixel62);
    m_pixels.append(ui->pixel63);
    m_pixels.append(ui->pixel64);
    m_pixels.append(ui->pixel65);
    m_pixels.append(ui->pixel66);
    m_pixels.append(ui->pixel67);
    m_pixels.append(ui->pixel68);

    m_pixels.append(ui->pixel71);
    m_pixels.append(ui->pixel72);
    m_pixels.append(ui->pixel73);
    m_pixels.append(ui->pixel74);
    m_pixels.append(ui->pixel75);
    m_pixels.append(ui->pixel76);
    m_pixels.append(ui->pixel77);
    m_pixels.append(ui->pixel78);

    m_pixels.append(ui->pixel81);
    m_pixels.append(ui->pixel82);
    m_pixels.append(ui->pixel83);
    m_pixels.append(ui->pixel84);
    m_pixels.append(ui->pixel85);
    m_pixels.append(ui->pixel86);
    m_pixels.append(ui->pixel87);
    m_pixels.append(ui->pixel88);

    m_pixels.append(ui->pixel91);
    m_pixels.append(ui->pixel92);
    m_pixels.append(ui->pixel93);
    m_pixels.append(ui->pixel94);
    m_pixels.append(ui->pixel95);
    m_pixels.append(ui->pixel96);
    m_pixels.append(ui->pixel97);
    m_pixels.append(ui->pixel98);

    m_pixels.append(ui->pixel101);
    m_pixels.append(ui->pixel102);
    m_pixels.append(ui->pixel103);
    m_pixels.append(ui->pixel104);
    m_pixels.append(ui->pixel105);
    m_pixels.append(ui->pixel106);
    m_pixels.append(ui->pixel107);
    m_pixels.append(ui->pixel108);
}

void MainWindow::on_pixel11_clicked()
{
    if (ui->pixel11->toolTip() == "white")
    {
        ui->pixel11->setIcon(m_black);
        ui->pixel11->setToolTip("black");
        m_inputs[0] = 1;
    }
    else
    {
        ui->pixel11->setIcon(m_white);
        ui->pixel11->setToolTip("white");
        m_inputs[0] = -1;
    }

}


void MainWindow::on_pixel12_clicked()
{
    if (ui->pixel12->toolTip() == "white")
    {
        ui->pixel12->setIcon(m_black);
        ui->pixel12->setToolTip("black");
        m_inputs[1] = 1;
    }
    else
    {
        ui->pixel12->setIcon(m_white);
        ui->pixel12->setToolTip("white");
        m_inputs[1] = -1;
    }

}

void MainWindow::on_pixel13_clicked()
{
    if (ui->pixel13->toolTip() == "white")
    {
        ui->pixel13->setIcon(m_black);
        ui->pixel13->setToolTip("black");
        m_inputs[2] = 1;
    }
    else
    {
        ui->pixel13->setIcon(m_white);
        ui->pixel13->setToolTip("white");
        m_inputs[2] = -1;
    }

}

void MainWindow::on_pixel14_clicked()
{
    if (ui->pixel14->toolTip() == "white")
    {
        ui->pixel14->setIcon(m_black);
        ui->pixel14->setToolTip("black");
        m_inputs[3] = 1;
    }
    else
    {
        ui->pixel14->setIcon(m_white);
        ui->pixel14->setToolTip("white");
        m_inputs[3] = -1;
    }

}

void MainWindow::on_pixel15_clicked()
{
    if (ui->pixel15->toolTip() == "white")
    {
        ui->pixel15->setIcon(m_black);
        ui->pixel15->setToolTip("black");
        m_inputs[4] = 1;
    }
    else
    {
        ui->pixel15->setIcon(m_white);
        ui->pixel15->setToolTip("white");
        m_inputs[4] = -1;
    }

}


void MainWindow::on_pixel16_clicked()
{
    if (ui->pixel16->toolTip() == "white")
    {
        ui->pixel16->setIcon(m_black);
        ui->pixel16->setToolTip("black");
        m_inputs[5] = 1;
    }
    else
    {
        ui->pixel16->setIcon(m_white);
        ui->pixel16->setToolTip("white");
        m_inputs[5] = -1;
    }

}

void MainWindow::on_pixel17_clicked()
{
    if (ui->pixel17->toolTip() == "white")
    {
        ui->pixel17->setIcon(m_black);
        ui->pixel17->setToolTip("black");
        m_inputs[6] = 1;
    }
    else
    {
        ui->pixel17->setIcon(m_white);
        ui->pixel17->setToolTip("white");
        m_inputs[6] = -1;
    }

}

void MainWindow::on_pixel18_clicked()
{
    if (ui->pixel18->toolTip() == "white")
    {
        ui->pixel18->setIcon(m_black);
        ui->pixel18->setToolTip("black");
        m_inputs[7] = 1;
    }
    else
    {
        ui->pixel18->setIcon(m_white);
        ui->pixel18->setToolTip("white");
        m_inputs[7] = -1;
    }

}

void MainWindow::on_pixel21_clicked()
{
    if (ui->pixel21->toolTip() == "white")
    {
        ui->pixel21->setIcon(m_black);
        ui->pixel21->setToolTip("black");
        m_inputs[8] = 1;
    }
    else
    {
        ui->pixel21->setIcon(m_white);
        ui->pixel21->setToolTip("white");
        m_inputs[8] = -1;
    }
}

void MainWindow::on_pixel22_clicked()
{
    if (ui->pixel22->toolTip() == "white")
    {
        ui->pixel22->setIcon(m_black);
        ui->pixel22->setToolTip("black");
        m_inputs[9] = 1;
    }
    else
    {
        ui->pixel22->setIcon(m_white);
        ui->pixel22->setToolTip("white");
        m_inputs[9] = -1;
    }
}


void MainWindow::on_pixel23_clicked()
{
    if (ui->pixel23->toolTip() == "white")
    {
        ui->pixel23->setIcon(m_black);
        ui->pixel23->setToolTip("black");
        m_inputs[10] = 1;
    }
    else
    {
        ui->pixel23->setIcon(m_white);
        ui->pixel23->setToolTip("white");
        m_inputs[10] = -1;
    }
}


void MainWindow::on_pixel24_clicked()
{
    if (ui->pixel24->toolTip() == "white")
    {
        ui->pixel24->setIcon(m_black);
        ui->pixel24->setToolTip("black");
        m_inputs[11] = 1;
    }
    else
    {
        ui->pixel24->setIcon(m_white);
        ui->pixel24->setToolTip("white");
        m_inputs[11] = -1;
    }
}


void MainWindow::on_pixel25_clicked()
{
    if (ui->pixel25->toolTip() == "white")
    {
        ui->pixel25->setIcon(m_black);
        ui->pixel25->setToolTip("black");
        m_inputs[12] = 1;
    }
    else
    {
        ui->pixel25->setIcon(m_white);
        ui->pixel25->setToolTip("white");
        m_inputs[12] = -1;
    }

}


void MainWindow::on_pixel26_clicked()
{
    if (ui->pixel26->toolTip() == "white")
    {
        ui->pixel26->setIcon(m_black);
        ui->pixel26->setToolTip("black");
        m_inputs[13] = 1;
    }
    else
    {
        ui->pixel26->setIcon(m_white);
        ui->pixel26->setToolTip("white");
        m_inputs[13] = -1;
    }

}

void MainWindow::on_pixel27_clicked()
{
    if (ui->pixel27->toolTip() == "white")
    {
        ui->pixel27->setIcon(m_black);
        ui->pixel27->setToolTip("black");
        m_inputs[14] = 1;
    }
    else
    {
        ui->pixel27->setIcon(m_white);
        ui->pixel27->setToolTip("white");
        m_inputs[14] = -1;
    }

}

void MainWindow::on_pixel28_clicked()
{
    if (ui->pixel28->toolTip() == "white")
    {
        ui->pixel28->setIcon(m_black);
        ui->pixel28->setToolTip("black");
        m_inputs[15] = 1;
    }
    else
    {
        ui->pixel28->setIcon(m_white);
        ui->pixel28->setToolTip("white");
        m_inputs[15] = -1;
    }

}



void MainWindow::on_pixel31_clicked()
{
    if (ui->pixel31->toolTip() == "white")
    {
        ui->pixel31->setIcon(m_black);
        ui->pixel31->setToolTip("black");
        m_inputs[16] = 1;
    }
    else
    {
        ui->pixel31->setIcon(m_white);
        ui->pixel31->setToolTip("white");
        m_inputs[16] = -1;
    }
}


void MainWindow::on_pixel32_clicked()
{
    if (ui->pixel32->toolTip() == "white")
    {
        ui->pixel32->setIcon(m_black);
        ui->pixel32->setToolTip("black");
        m_inputs[17] = 1;
    }
    else
    {
        ui->pixel32->setIcon(m_white);
        ui->pixel32->setToolTip("white");
        m_inputs[17] = -1;
    }
}


void MainWindow::on_pixel33_clicked()
{
    if (ui->pixel33->toolTip() == "white")
    {
        ui->pixel33->setIcon(m_black);
        ui->pixel33->setToolTip("black");
        m_inputs[18] = 1;
    }
    else
    {
        ui->pixel33->setIcon(m_white);
        ui->pixel33->setToolTip("white");
        m_inputs[18] = -1;
    }
}


void MainWindow::on_pixel34_clicked()
{
    if (ui->pixel34->toolTip() == "white")
    {
        ui->pixel34->setIcon(m_black);
        ui->pixel34->setToolTip("black");
        m_inputs[19] = 1;
    }
    else
    {
        ui->pixel34->setIcon(m_white);
        ui->pixel34->setToolTip("white");
        m_inputs[19] = -1;
    }
}


void MainWindow::on_pixel35_clicked()
{
    if (ui->pixel35->toolTip() == "white")
    {
        ui->pixel35->setIcon(m_black);
        ui->pixel35->setToolTip("black");
        m_inputs[20] = 1;
    }
    else
    {
        ui->pixel35->setIcon(m_white);
        ui->pixel35->setToolTip("white");
        m_inputs[20] = -1;
    }

}


void MainWindow::on_pixel36_clicked()
{
    if (ui->pixel36->toolTip() == "white")
    {
        ui->pixel36->setIcon(m_black);
        ui->pixel36->setToolTip("black");
        m_inputs[21] = 1;
    }
    else
    {
        ui->pixel36->setIcon(m_white);
        ui->pixel36->setToolTip("white");
        m_inputs[21] = -1;
    }

}

void MainWindow::on_pixel37_clicked()
{
    if (ui->pixel37->toolTip() == "white")
    {
        ui->pixel37->setIcon(m_black);
        ui->pixel37->setToolTip("black");
        m_inputs[22] = 1;
    }
    else
    {
        ui->pixel37->setIcon(m_white);
        ui->pixel37->setToolTip("white");
        m_inputs[22] = -1;
    }

}

void MainWindow::on_pixel38_clicked()
{
    if (ui->pixel38->toolTip() == "white")
    {
        ui->pixel38->setIcon(m_black);
        ui->pixel38->setToolTip("black");
        m_inputs[23] = 1;
    }
    else
    {
        ui->pixel38->setIcon(m_white);
        ui->pixel38->setToolTip("white");
        m_inputs[23] = -1;
    }

}


void MainWindow::on_pixel41_clicked()
{
    if (ui->pixel41->toolTip() == "white")
    {
        ui->pixel41->setIcon(m_black);
        ui->pixel41->setToolTip("black");
        m_inputs[24] = 1;
    }
    else
    {
        ui->pixel41->setIcon(m_white);
        ui->pixel41->setToolTip("white");
        m_inputs[24] = -1;
    }
}


void MainWindow::on_pixel42_clicked()
{
    if (ui->pixel42->toolTip() == "white")
    {
        ui->pixel42->setIcon(m_black);
        ui->pixel42->setToolTip("black");
        m_inputs[25] = 1;
    }
    else
    {
        ui->pixel42->setIcon(m_white);
        ui->pixel42->setToolTip("white");
        m_inputs[25] = -1;
    }
}


void MainWindow::on_pixel43_clicked()
{
    if (ui->pixel43->toolTip() == "white")
    {
        ui->pixel43->setIcon(m_black);
        ui->pixel43->setToolTip("black");
        m_inputs[26] = 1;
    }
    else
    {
        ui->pixel43->setIcon(m_white);
        ui->pixel43->setToolTip("white");
        m_inputs[26] = -1;
    }
}


void MainWindow::on_pixel44_clicked()
{
    if (ui->pixel44->toolTip() == "white")
    {
        ui->pixel44->setIcon(m_black);
        ui->pixel44->setToolTip("black");
        m_inputs[27] = 1;
    }
    else
    {
        ui->pixel44->setIcon(m_white);
        ui->pixel44->setToolTip("white");
        m_inputs[27] = -1;
    }
}


void MainWindow::on_pixel45_clicked()
{
    if (ui->pixel45->toolTip() == "white")
    {
        ui->pixel45->setIcon(m_black);
        ui->pixel45->setToolTip("black");
        m_inputs[28] = 1;
    }
    else
    {
        ui->pixel45->setIcon(m_white);
        ui->pixel45->setToolTip("white");
        m_inputs[28] = -1;
    }

}


void MainWindow::on_pixel46_clicked()
{
    if (ui->pixel46->toolTip() == "white")
    {
        ui->pixel46->setIcon(m_black);
        ui->pixel46->setToolTip("black");
        m_inputs[29] = 1;
    }
    else
    {
        ui->pixel46->setIcon(m_white);
        ui->pixel46->setToolTip("white");
        m_inputs[29] = -1;
    }

}

void MainWindow::on_pixel47_clicked()
{
    if (ui->pixel47->toolTip() == "white")
    {
        ui->pixel47->setIcon(m_black);
        ui->pixel47->setToolTip("black");
        m_inputs[30] = 1;
    }
    else
    {
        ui->pixel47->setIcon(m_white);
        ui->pixel47->setToolTip("white");
        m_inputs[30] = -1;
    }

}

void MainWindow::on_pixel48_clicked()
{
    if (ui->pixel48->toolTip() == "white")
    {
        ui->pixel48->setIcon(m_black);
        ui->pixel48->setToolTip("black");
        m_inputs[31] = 1;
    }
    else
    {
        ui->pixel48->setIcon(m_white);
        ui->pixel48->setToolTip("white");
        m_inputs[31] = -1;
    }

}


void MainWindow::on_pixel51_clicked()
{
    if (ui->pixel51->toolTip() == "white")
    {
        ui->pixel51->setIcon(m_black);
        ui->pixel51->setToolTip("black");
        m_inputs[32] = 1;
    }
    else
    {
        ui->pixel51->setIcon(m_white);
        ui->pixel51->setToolTip("white");
        m_inputs[32] = -1;
    }
}


void MainWindow::on_pixel52_clicked()
{
    if (ui->pixel52->toolTip() == "white")
    {
        ui->pixel52->setIcon(m_black);
        ui->pixel52->setToolTip("black");
        m_inputs[33] = 1;
    }
    else
    {
        ui->pixel52->setIcon(m_white);
        ui->pixel52->setToolTip("white");
        m_inputs[33] = -1;
    }
}


void MainWindow::on_pixel53_clicked()
{
    if (ui->pixel53->toolTip() == "white")
    {
        ui->pixel53->setIcon(m_black);
        ui->pixel53->setToolTip("black");
        m_inputs[34] = 1;
    }
    else
    {
        ui->pixel53->setIcon(m_white);
        ui->pixel53->setToolTip("white");
        m_inputs[34] = -1;
    }
}


void MainWindow::on_pixel54_clicked()
{
    if (ui->pixel54->toolTip() == "white")
    {
        ui->pixel54->setIcon(m_black);
        ui->pixel54->setToolTip("black");
        m_inputs[35] = 1;
    }
    else
    {
        ui->pixel54->setIcon(m_white);
        ui->pixel54->setToolTip("white");
        m_inputs[35] = -1;
    }
}

void MainWindow::on_pixel55_clicked()
{
    if (ui->pixel55->toolTip() == "white")
    {
        ui->pixel55->setIcon(m_black);
        ui->pixel55->setToolTip("black");
        m_inputs[36] = 1;
    }
    else
    {
        ui->pixel55->setIcon(m_white);
        ui->pixel55->setToolTip("white");
        m_inputs[36] = -1;
    }

}


void MainWindow::on_pixel56_clicked()
{
    if (ui->pixel56->toolTip() == "white")
    {
        ui->pixel56->setIcon(m_black);
        ui->pixel56->setToolTip("black");
        m_inputs[37] = 1;
    }
    else
    {
        ui->pixel56->setIcon(m_white);
        ui->pixel56->setToolTip("white");
        m_inputs[37] = -1;
    }

}

void MainWindow::on_pixel57_clicked()
{
    if (ui->pixel57->toolTip() == "white")
    {
        ui->pixel57->setIcon(m_black);
        ui->pixel57->setToolTip("black");
        m_inputs[38] = 1;
    }
    else
    {
        ui->pixel57->setIcon(m_white);
        ui->pixel57->setToolTip("white");
        m_inputs[38] = -1;
    }

}

void MainWindow::on_pixel58_clicked()
{
    if (ui->pixel58->toolTip() == "white")
    {
        ui->pixel58->setIcon(m_black);
        ui->pixel58->setToolTip("black");
        m_inputs[39] = 1;
    }
    else
    {
        ui->pixel58->setIcon(m_white);
        ui->pixel58->setToolTip("white");
        m_inputs[39] = -1;
    }

}


void MainWindow::on_pixel61_clicked()
{
    if (ui->pixel61->toolTip() == "white")
    {
        ui->pixel61->setIcon(m_black);
        ui->pixel61->setToolTip("black");
        m_inputs[40] = 1;
    }
    else
    {
        ui->pixel61->setIcon(m_white);
        ui->pixel61->setToolTip("white");
        m_inputs[40] = -1;
    }
}


void MainWindow::on_pixel62_clicked()
{
    if (ui->pixel62->toolTip() == "white")
    {
        ui->pixel62->setIcon(m_black);
        ui->pixel62->setToolTip("black");
        m_inputs[41] = 1;
    }
    else
    {
        ui->pixel62->setIcon(m_white);
        ui->pixel62->setToolTip("white");
        m_inputs[41] = -1;
    }
}


void MainWindow::on_pixel63_clicked()
{
    if (ui->pixel63->toolTip() == "white")
    {
        ui->pixel63->setIcon(m_black);
        ui->pixel63->setToolTip("black");
        m_inputs[42] = 1;
    }
    else
    {
        ui->pixel63->setIcon(m_white);
        ui->pixel63->setToolTip("white");
        m_inputs[42] = -1;
    }
}


void MainWindow::on_pixel64_clicked()
{
    if (ui->pixel64->toolTip() == "white")
    {
        ui->pixel64->setIcon(m_black);
        ui->pixel64->setToolTip("black");
        m_inputs[43] = 1;
    }
    else
    {
        ui->pixel64->setIcon(m_white);
        ui->pixel64->setToolTip("white");
        m_inputs[43] = -1;
    }
}

void MainWindow::on_pixel65_clicked()
{
    if (ui->pixel65->toolTip() == "white")
    {
        ui->pixel65->setIcon(m_black);
        ui->pixel65->setToolTip("black");
        m_inputs[44] = 1;
    }
    else
    {
        ui->pixel65->setIcon(m_white);
        ui->pixel65->setToolTip("white");
        m_inputs[44] = -1;
    }

}


void MainWindow::on_pixel66_clicked()
{
    if (ui->pixel66->toolTip() == "white")
    {
        ui->pixel66->setIcon(m_black);
        ui->pixel66->setToolTip("black");
        m_inputs[45] = 1;
    }
    else
    {
        ui->pixel66->setIcon(m_white);
        ui->pixel66->setToolTip("white");
        m_inputs[45] = -1;
    }

}

void MainWindow::on_pixel67_clicked()
{
    if (ui->pixel67->toolTip() == "white")
    {
        ui->pixel67->setIcon(m_black);
        ui->pixel67->setToolTip("black");
        m_inputs[46] = 1;
    }
    else
    {
        ui->pixel67->setIcon(m_white);
        ui->pixel67->setToolTip("white");
        m_inputs[46] = -1;
    }

}

void MainWindow::on_pixel68_clicked()
{
    if (ui->pixel68->toolTip() == "white")
    {
        ui->pixel68->setIcon(m_black);
        ui->pixel68->setToolTip("black");
        m_inputs[47] = 1;
    }
    else
    {
        ui->pixel68->setIcon(m_white);
        ui->pixel68->setToolTip("white");
        m_inputs[47] = -1;
    }

}


void MainWindow::on_pixel71_clicked()
{
    if (ui->pixel71->toolTip() == "white")
    {
        ui->pixel71->setIcon(m_black);
        ui->pixel71->setToolTip("black");
        m_inputs[48] = 1;
    }
    else
    {
        ui->pixel71->setIcon(m_white);
        ui->pixel71->setToolTip("white");
        m_inputs[48] = -1;
    }
}


void MainWindow::on_pixel72_clicked()
{
    if (ui->pixel72->toolTip() == "white")
    {
        ui->pixel72->setIcon(m_black);
        ui->pixel72->setToolTip("black");
        m_inputs[49] = 1;
    }
    else
    {
        ui->pixel72->setIcon(m_white);
        ui->pixel72->setToolTip("white");
        m_inputs[49] = -1;
    }
}


void MainWindow::on_pixel73_clicked()
{
    if (ui->pixel73->toolTip() == "white")
    {
        ui->pixel73->setIcon(m_black);
        ui->pixel73->setToolTip("black");
        m_inputs[50] = 1;
    }
    else
    {
        ui->pixel73->setIcon(m_white);
        ui->pixel73->setToolTip("white");
        m_inputs[50] = -1;
    }
}


void MainWindow::on_pixel74_clicked()
{
    if (ui->pixel74->toolTip() == "white")
    {
        ui->pixel74->setIcon(m_black);
        ui->pixel74->setToolTip("black");
        m_inputs[51] = 1;
    }
    else
    {
        ui->pixel74->setIcon(m_white);
        ui->pixel74->setToolTip("white");
        m_inputs[51] = -1;
    }
}

void MainWindow::on_pixel75_clicked()
{
    if (ui->pixel75->toolTip() == "white")
    {
        ui->pixel75->setIcon(m_black);
        ui->pixel75->setToolTip("black");
        m_inputs[52] = 1;
    }
    else
    {
        ui->pixel75->setIcon(m_white);
        ui->pixel75->setToolTip("white");
        m_inputs[52] = -1;
    }

}


void MainWindow::on_pixel76_clicked()
{
    if (ui->pixel76->toolTip() == "white")
    {
        ui->pixel76->setIcon(m_black);
        ui->pixel76->setToolTip("black");
        m_inputs[53] = 1;
    }
    else
    {
        ui->pixel76->setIcon(m_white);
        ui->pixel76->setToolTip("white");
        m_inputs[53] = -1;
    }

}

void MainWindow::on_pixel77_clicked()
{
    if (ui->pixel77->toolTip() == "white")
    {
        ui->pixel77->setIcon(m_black);
        ui->pixel77->setToolTip("black");
        m_inputs[54] = 1;
    }
    else
    {
        ui->pixel77->setIcon(m_white);
        ui->pixel77->setToolTip("white");
        m_inputs[54] = -1;
    }

}

void MainWindow::on_pixel78_clicked()
{
    if (ui->pixel78->toolTip() == "white")
    {
        ui->pixel78->setIcon(m_black);
        ui->pixel78->setToolTip("black");
        m_inputs[55] = 1;
    }
    else
    {
        ui->pixel78->setIcon(m_white);
        ui->pixel78->setToolTip("white");
        m_inputs[55] = -1;
    }

}

void MainWindow::on_pixel81_clicked()
{
    if (ui->pixel81->toolTip() == "white")
    {
        ui->pixel81->setIcon(m_black);
        ui->pixel81->setToolTip("black");
        m_inputs[56] = 1;
    }
    else
    {
        ui->pixel81->setIcon(m_white);
        ui->pixel81->setToolTip("white");
        m_inputs[56] = -1;
    }
}


void MainWindow::on_pixel82_clicked()
{
    if (ui->pixel82->toolTip() == "white")
    {
        ui->pixel82->setIcon(m_black);
        ui->pixel82->setToolTip("black");
        m_inputs[57] = 1;
    }
    else
    {
        ui->pixel82->setIcon(m_white);
        ui->pixel82->setToolTip("white");
        m_inputs[57] = -1;
    }
}


void MainWindow::on_pixel83_clicked()
{
    if (ui->pixel83->toolTip() == "white")
    {
        ui->pixel83->setIcon(m_black);
        ui->pixel83->setToolTip("black");
        m_inputs[58] = 1;
    }
    else
    {
        ui->pixel83->setIcon(m_white);
        ui->pixel83->setToolTip("white");
        m_inputs[58] = -1;
    }
}


void MainWindow::on_pixel84_clicked()
{
    if (ui->pixel84->toolTip() == "white")
    {
        ui->pixel84->setIcon(m_black);
        ui->pixel84->setToolTip("black");
        m_inputs[59] = 1;
    }
    else
    {
        ui->pixel84->setIcon(m_white);
        ui->pixel84->setToolTip("white");
        m_inputs[59] = -1;
    }
}

void MainWindow::on_pixel85_clicked()
{
    if (ui->pixel85->toolTip() == "white")
    {
        ui->pixel85->setIcon(m_black);
        ui->pixel85->setToolTip("black");
        m_inputs[60] = 1;
    }
    else
    {
        ui->pixel85->setIcon(m_white);
        ui->pixel85->setToolTip("white");
        m_inputs[60] = -1;
    }

}


void MainWindow::on_pixel86_clicked()
{
    if (ui->pixel86->toolTip() == "white")
    {
        ui->pixel86->setIcon(m_black);
        ui->pixel86->setToolTip("black");
        m_inputs[61] = 1;
    }
    else
    {
        ui->pixel86->setIcon(m_white);
        ui->pixel86->setToolTip("white");
        m_inputs[61] = -1;
    }

}

void MainWindow::on_pixel87_clicked()
{
    if (ui->pixel87->toolTip() == "white")
    {
        ui->pixel87->setIcon(m_black);
        ui->pixel87->setToolTip("black");
        m_inputs[62] = 1;
    }
    else
    {
        ui->pixel87->setIcon(m_white);
        ui->pixel87->setToolTip("white");
        m_inputs[62] = -1;
    }

}

void MainWindow::on_pixel88_clicked()
{
    if (ui->pixel88->toolTip() == "white")
    {
        ui->pixel88->setIcon(m_black);
        ui->pixel88->setToolTip("black");
        m_inputs[63] = 1;
    }
    else
    {
        ui->pixel88->setIcon(m_white);
        ui->pixel88->setToolTip("white");
        m_inputs[63] = -1;
    }

}

void MainWindow::on_pixel91_clicked()
{
    if (ui->pixel91->toolTip() == "white")
    {
        ui->pixel91->setIcon(m_black);
        ui->pixel91->setToolTip("black");
        m_inputs[64] = 1;
    }
    else
    {
        ui->pixel91->setIcon(m_white);
        ui->pixel91->setToolTip("white");
        m_inputs[64] = -1;
    }
}


void MainWindow::on_pixel92_clicked()
{
    if (ui->pixel92->toolTip() == "white")
    {
        ui->pixel92->setIcon(m_black);
        ui->pixel92->setToolTip("black");
        m_inputs[65] = 1;
    }
    else
    {
        ui->pixel92->setIcon(m_white);
        ui->pixel92->setToolTip("white");
        m_inputs[65] = -1;
    }
}


void MainWindow::on_pixel93_clicked()
{
    if (ui->pixel93->toolTip() == "white")
    {
        ui->pixel93->setIcon(m_black);
        ui->pixel93->setToolTip("black");
        m_inputs[66] = 1;
    }
    else
    {
        ui->pixel93->setIcon(m_white);
        ui->pixel93->setToolTip("white");
        m_inputs[66] = -1;
    }
}


void MainWindow::on_pixel94_clicked()
{
    if (ui->pixel94->toolTip() == "white")
    {
        ui->pixel94->setIcon(m_black);
        ui->pixel94->setToolTip("black");
        m_inputs[67] = 1;
    }
    else
    {
        ui->pixel94->setIcon(m_white);
        ui->pixel94->setToolTip("white");
        m_inputs[67] = -1;
    }
}

void MainWindow::on_pixel95_clicked()
{
    if (ui->pixel95->toolTip() == "white")
    {
        ui->pixel95->setIcon(m_black);
        ui->pixel95->setToolTip("black");
        m_inputs[68] = 1;
    }
    else
    {
        ui->pixel95->setIcon(m_white);
        ui->pixel95->setToolTip("white");
        m_inputs[68] = -1;
    }

}


void MainWindow::on_pixel96_clicked()
{
    if (ui->pixel96->toolTip() == "white")
    {
        ui->pixel96->setIcon(m_black);
        ui->pixel96->setToolTip("black");
        m_inputs[69] = 1;
    }
    else
    {
        ui->pixel96->setIcon(m_white);
        ui->pixel96->setToolTip("white");
        m_inputs[69] = -1;
    }

}

void MainWindow::on_pixel97_clicked()
{
    if (ui->pixel97->toolTip() == "white")
    {
        ui->pixel97->setIcon(m_black);
        ui->pixel97->setToolTip("black");
        m_inputs[70] = 1;
    }
    else
    {
        ui->pixel97->setIcon(m_white);
        ui->pixel97->setToolTip("white");
        m_inputs[70] = -1;
    }

}

void MainWindow::on_pixel98_clicked()
{
    if (ui->pixel98->toolTip() == "white")
    {
        ui->pixel98->setIcon(m_black);
        ui->pixel98->setToolTip("black");
        m_inputs[71] = 1;
    }
    else
    {
        ui->pixel98->setIcon(m_white);
        ui->pixel98->setToolTip("white");
        m_inputs[71] = -1;
    }

}

void MainWindow::on_pixel101_clicked()
{
    if (ui->pixel101->toolTip() == "white")
    {
        ui->pixel101->setIcon(m_black);
        ui->pixel101->setToolTip("black");
        m_inputs[72] = 1;
    }
    else
    {
        ui->pixel101->setIcon(m_white);
        ui->pixel101->setToolTip("white");
        m_inputs[72] = -1;
    }
}


void MainWindow::on_pixel102_clicked()
{
    if (ui->pixel102->toolTip() == "white")
    {
        ui->pixel102->setIcon(m_black);
        ui->pixel102->setToolTip("black");
        m_inputs[73] = 1;
    }
    else
    {
        ui->pixel102->setIcon(m_white);
        ui->pixel102->setToolTip("white");
        m_inputs[73] = -1;
    }
}


void MainWindow::on_pixel103_clicked()
{
    if (ui->pixel103->toolTip() == "white")
    {
        ui->pixel103->setIcon(m_black);
        ui->pixel103->setToolTip("black");
        m_inputs[74] = 1;
    }
    else
    {
        ui->pixel103->setIcon(m_white);
        ui->pixel103->setToolTip("white");
        m_inputs[74] = -1;
    }
}


void MainWindow::on_pixel104_clicked()
{
    if (ui->pixel104->toolTip() == "white")
    {
        ui->pixel104->setIcon(m_black);
        ui->pixel104->setToolTip("black");
        m_inputs[75] = 1;
    }
    else
    {
        ui->pixel104->setIcon(m_white);
        ui->pixel104->setToolTip("white");
        m_inputs[75] = -1;
    }
}

void MainWindow::on_pixel105_clicked()
{
    if (ui->pixel105->toolTip() == "white")
    {
        ui->pixel105->setIcon(m_black);
        ui->pixel105->setToolTip("black");
        m_inputs[76] = 1;
    }
    else
    {
        ui->pixel105->setIcon(m_white);
        ui->pixel105->setToolTip("white");
        m_inputs[76] = -1;
    }

}


void MainWindow::on_pixel106_clicked()
{
    if (ui->pixel106->toolTip() == "white")
    {
        ui->pixel106->setIcon(m_black);
        ui->pixel106->setToolTip("black");
        m_inputs[77] = 1;
    }
    else
    {
        ui->pixel106->setIcon(m_white);
        ui->pixel106->setToolTip("white");
        m_inputs[77] = -1;
    }

}

void MainWindow::on_pixel107_clicked()
{
    if (ui->pixel107->toolTip() == "white")
    {
        ui->pixel107->setIcon(m_black);
        ui->pixel107->setToolTip("black");
        m_inputs[78] = 1;
    }
    else
    {
        ui->pixel107->setIcon(m_white);
        ui->pixel107->setToolTip("white");
        m_inputs[78] = -1;
    }

}

void MainWindow::on_pixel108_clicked()
{
    if (ui->pixel108->toolTip() == "white")
    {
        ui->pixel108->setIcon(m_black);
        ui->pixel108->setToolTip("black");
        m_inputs[79] = 1;
    }
    else
    {
        ui->pixel108->setIcon(m_white);
        ui->pixel108->setToolTip("white");
        m_inputs[79] = -1;
    }

}


void MainWindow::UpdateDisplay(QVector<int> inputs)
{
    for(int i = 0; i < m_pixels.size(); i++)
    {
        if(inputs.at(i) == 1)
        {
            m_pixels.at(i)->setIcon(m_black);
            m_pixels.at(i)->setToolTip("black");
        }
        else
        {
            m_pixels.at(i)->setIcon(m_white);
            m_pixels.at(i)->setToolTip("white");
        }
    }
}

void MainWindow::PopulateTrainingSet()
{

    m_fontA = QVector<QVector<int>>();

    /*
   * INPUT.
   * Each line describes a number.
   * 1 - pixel is black
   * -1 - pixel is white
   */

    // #0
    QVector<int> number0 = QVector<int>({-1, -1, 1, 1, 1, 1, -1, -1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1 , -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         -1, -1, 1, 1, 1, 1, -1, -1,});
    m_fontA.append(number0);

    // #1
    QVector<int> number1 = QVector<int>({-1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, 1, 1, 1, 1, -1, -1, -1,
                                         -1, 1, 1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,});
    m_fontA.append(number1);

    //#2
    QVector<int> number2 = QVector<int>({-1, -1, 1, 1, 1, 1, -1, -1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         -1, -1, -1, -1, -1, 1, 1, -1,
                                         -1, -1, -1, -1, 1, 1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, 1, 1, -1, -1, -1, -1,
                                         1, 1, 1, 1, 1, 1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, 1,});
    m_fontA.append(number2);

    //#3
    QVector<int> number3 = QVector<int>({1, 1, 1, 1, 1, 1, -1, -1,
                                         1, 1, 1, 1, 1, 1, 1, -1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         -1, -1, -1, -1, 1, 1, 1, -1,
                                         -1, -1, -1, -1, 1, 1, 1, -1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, 1, 1, 1, 1, -1, -1});
    m_fontA.append(number3);

    //#4
    QVector<int> number4 = QVector<int>({1, 1, -1, -1, 1, 1, -1, -1,
                                         1, 1, -1, -1, 1, 1, -1, -1,
                                         1, 1, -1, -1, 1, 1, -1, -1,
                                         1, 1, -1, -1, 1, 1, -1, -1,
                                         1, 1, 1, 1, 1, 1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, 1,
                                         -1, -1, -1, -1, 1, 1, -1, -1,
                                         -1, -1, -1, -1, 1, 1, -1, -1,
                                         -1, -1, -1, -1, 1, 1, -1, -1,
                                         -1, -1, -1, -1, 1, 1, -1, -1});
    m_fontA.append(number4);

    //#5
    QVector<int> number5 = QVector<int>({1, 1, 1, 1, 1, 1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, 1,
                                         1, 1, -1, -1, -1, -1, -1, -1,
                                         1, 1, -1, -1, -1, -1, -1, -1,
                                         1, 1, 1, 1, 1, 1, -1, -1,
                                         1, 1, 1, 1, 1, 1, 1, -1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, 1, 1, 1, 1, -1, -1});
    m_fontA.append(number5);

    //#6
    QVector<int> number6 = QVector<int>({-1, -1, 1, 1, 1, 1, 1, 1,
                                         -1, 1, 1, 1, 1, 1, 1, 1,
                                         1, 1, -1, -1, -1, -1, -1, -1,
                                         1, 1, -1, -1, -1, -1, -1, -1,
                                         1, 1, 1, 1, 1, 1, -1, -1,
                                         1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, -1,
                                         -1, 1, 1, 1, 1, 1, -1, -1});
    m_fontA.append(number6);

    //#7
    QVector<int> number7 = QVector<int>({1, 1, 1, 1, 1, 1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, 1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         -1, -1, -1, -1, -1, 1, 1, 1,
                                         -1, -1, -1, -1, 1, 1, 1, -1,
                                         -1, -1, -1, 1, 1, 1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1,
                                         -1, -1, -1, 1, 1, -1, -1, -1});
    m_fontA.append(number7);

    //#8
    QVector<int> number8 = QVector<int>({-1, -1, 1, 1, 1, 1, -1, -1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         -1, -1, 1, 1, 1, 1, -1, -1});
    m_fontA.append(number8);

    //#9
    QVector<int> number9 = QVector<int>({-1, -1, 1, 1, 1, 1, -1, -1,
                                         -1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         1, 1, -1, -1, -1, -1, 1, 1,
                                         -1, 1, 1, 1, 1, 1, 1, 1,
                                         -1, -1, 1, 1, 1, 1, 1, 1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         -1, -1, -1, -1, -1, -1, 1, 1,
                                         1, 1, 1, 1, 1, 1, 1, -1,
                                         1, 1, 1, 1, 1, 1, -1, -1});
    m_fontA.append(number9);


    //Segunda fonte

    m_fontB = QVector<QVector<int>>();


    QVector<int> number0n = QVector<int>({

                                             1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1,
                                             -1, 1, 1, 1, -1, -1, -1, -1, -1, 1, 1, -1, 1,
                                             -1, -1, -1, -1, 1, 1, -1, -1, 1, -1, -1, -1, 1,
                                             1, -1, -1, -1, 1, -1, -1, 1, 1, -1, -1, -1, -1,
                                             1, -1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, -1, -1,
                                             -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1}

                                         );

    m_fontB.append(number0n);


    QVector<int> number1n = QVector<int>({

                                             -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, -1,
                                             -1, -1, -1, 1, 1, 1, 1, -1, -1, -1, 1, 1, 1, 1, 1,
                                             -1, -1, -1, 1, 1, -1, 1, 1, -1, -1, -1, -1, -1, -1,
                                             1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1,
                                             -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1,
                                             -1, -1, -1, -1, 1, 1, -1, -1, -1}

                                         );

    m_fontB.append(number1n);


    QVector<int> number2n = QVector<int>({

                                             -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1,
                                             -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, -1, -1,
                                             -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1,
                                             1, 1, 1, 1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1,
                                             -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1,
                                             -1, -1, -1, 1, 1, 1, 1, -1, -1}

                                         );

    m_fontB.append(number2n);


    QVector<int> number3n = QVector<int>({

                                             1, 1, 1, 1, 1, 1, 1, -1, 1, 1, -1, -1, -1, -1, 1, -1,
                                             1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1,
                                             1, -1, -1, -1, 1, 1, 1, 1, 1, -1, -1, -1, 1, 1, 1, 1,
                                             1, -1, -1, -1, -1, -1, -1, -1, 1, -1, 1, -1, -1, -1,
                                             -1, -1, 1, -1, 1, 1, -1, -1, -1, -1, 1, -1, 1, 1, 1,
                                             1, 1, 1, 1, -1}

                                         );

    m_fontB.append(number3n);


    QVector<int> number4n = QVector<int>({

                                             1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, 1, 1, -1, -1, -1, 1, -1, -1, 1, 1, -1, -1, -1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1}

                                         );

    m_fontB.append(number4n);


    QVector<int> number5n = QVector<int>({

                                             -1, -1, 1, 1, 1, 1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, 1, 1, 1, 1, -1, -1}

                                         );

    m_fontB.append(number5n);


    QVector<int> number6n = QVector<int>({

                                             -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, 1, 1, 1, 1, -1}

                                         );

    m_fontB.append(number6n);


    QVector<int> number7n = QVector<int>({

                                             -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1}

                                         );

    m_fontB.append(number7n);


    QVector<int> number8n = QVector<int>({

                                             -1, 1, 1, 1, 1, 1, 1, -1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, -1, 1, 1, 1, 1, 1, 1, -1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, -1, 1, 1, 1, 1, 1, 1, -1}

                                         );

    m_fontB.append(number8n);


    QVector<int> number9n = QVector<int>({

                                             -1, 1, 1, 1, 1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, 1, -1, -1, -1, -1, 1, -1, -1, 1, 1, 1, 1, 1, 1, -1}

                                         );

    m_fontB.append(number9n);


















    /* trainingList for each Neuron.
   * For example: neuron  #0 must be activated (output = 1) only with the first training pair,
   * corresponding to the number #0.
   * For the other training pairs (of other numbers), this neuron must
   * present output equal to -1.
   */

    // Neuron #0
    QVector<QPair<QVector<int>, int>> trainingList;
    trainingList.append(qMakePair(number0, 1));   //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9

    trainingList.append(qMakePair(number0n, 1));   //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #1
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, 1));   //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9

    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, 1));   //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #2
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, 1));   //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9

    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, 1));   //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #3
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, 1));   //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9


    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, 1));   //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #4
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, 1));   //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9


    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, 1));   //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #5
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, 1));   //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9


    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, 1));   //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #6
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, 1));   //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9


    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, 1));   //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #7
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, 1));   //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9


    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, 1));   //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #8
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, 1));   //pair9 = #8
    trainingList.append(qMakePair(number9, -1));  //pair10 = #9


    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, 1));   //pair9 = #8
    trainingList.append(qMakePair(number9n, -1));  //pair10 = #9
    m_trainingSet.append(trainingList);

    // Neuron #9
    trainingList = QVector<QPair<QVector<int>, int>>();
    trainingList.append(qMakePair(number0, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9, 1));   //pair10 = #9


    trainingList.append(qMakePair(number0n, -1));  //pair1 = #0
    trainingList.append(qMakePair(number1n, -1));  //pair2 = #1
    trainingList.append(qMakePair(number2n, -1));  //pair3 = #2
    trainingList.append(qMakePair(number3n, -1));  //pair4 = #3
    trainingList.append(qMakePair(number4n, -1));  //pair5 = #4
    trainingList.append(qMakePair(number5n, -1));  //pair6 = #5
    trainingList.append(qMakePair(number6n, -1));  //pair7 = #6
    trainingList.append(qMakePair(number7n, -1));  //pair8 = #7
    trainingList.append(qMakePair(number8n, -1));  //pair9 = #8
    trainingList.append(qMakePair(number9n, 1));   //pair10 = #9
    m_trainingSet.append(trainingList);


}

void MainWindow::on_pushButtonAprender_clicked()
{


    Perceptron *myPerceptron = new Perceptron();


    for(int i = 0; i < m_trainingSet.size(); i++ ){
        myPerceptron->UpdateParameters(m_trainingSet.at(i),0,0,1);
        QVector<double> peso = myPerceptron->CalculateWeights();
        m_neuronio.append(new Neuronio(1,myPerceptron->GetBias(),0,peso));

    }
    ui->statusBar->showMessage("Treinado");
    ui->pushButtonAprender->setEnabled(false);
}

void MainWindow::on_pushButtonRodar_clicked()
{
    qDebug()<<"\n\n"<<m_inputs;
    QVector<int> resultado;

    for(int i = 0; i < m_neuronio.size(); i++){
        if(m_neuronio.at(i)->CalculateOutput(m_inputs) == 1){
            //Reconheceu
            if (i > 9){
                resultado.append(i);
            }else{
                resultado.append(i);
            }
        }

    }
    QString sResultado;
    for(int i = 0; i < resultado.size(); i++){
        sResultado+= QString::number(resultado.at(i))+" ";
    }
    ui->statusBar->showMessage("Resultado: "+ sResultado);


}

void MainWindow::on_comboBoxFonte2_currentIndexChanged(int index)
{
    if (index == 0)
    {
        QVector<int> aux;
        for(int i = 0; i < 80; i++)
            aux.append(-1);

        UpdateDisplay(aux);
        m_inputs = aux;
    }
    else
    {
        UpdateDisplay(m_fontB.at(index - 1));
        m_inputs = m_fontB.at(index - 1);
    }
}
