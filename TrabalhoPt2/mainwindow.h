#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QIcon>
#include <QPushButton>
#include <QVector>
#include <QWindow>
#include <QMap>
#include <QPair>
#include <QDebug>
#include "perceptron.h"
#include "neuronio.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_characterComboBox_currentIndexChanged(int index);
  void on_pixel11_clicked();
  void on_pixel12_clicked();
  void on_pixel13_clicked();
  void on_pixel14_clicked();
  void on_pixel15_clicked();
  void on_pixel16_clicked();
  void on_pixel17_clicked();
  void on_pixel18_clicked();
  void on_pixel21_clicked();
  void on_pixel22_clicked();
  void on_pixel23_clicked();
  void on_pixel24_clicked();
  void on_pixel25_clicked();
  void on_pixel26_clicked();
  void on_pixel27_clicked();
  void on_pixel28_clicked();
  void on_pixel31_clicked();
  void on_pixel32_clicked();
  void on_pixel33_clicked();
  void on_pixel34_clicked();
  void on_pixel35_clicked();
  void on_pixel36_clicked();
  void on_pixel37_clicked();
  void on_pixel38_clicked();
  void on_pixel41_clicked();
  void on_pixel42_clicked();
  void on_pixel43_clicked();
  void on_pixel44_clicked();
  void on_pixel45_clicked();
  void on_pixel46_clicked();
  void on_pixel47_clicked();
  void on_pixel48_clicked();
  void on_pixel51_clicked();
  void on_pixel52_clicked();
  void on_pixel53_clicked();
  void on_pixel54_clicked();
  void on_pixel55_clicked();
  void on_pixel56_clicked();
  void on_pixel57_clicked();
  void on_pixel58_clicked();
  void on_pixel61_clicked();
  void on_pixel62_clicked();
  void on_pixel63_clicked();
  void on_pixel64_clicked();
  void on_pixel65_clicked();
  void on_pixel66_clicked();
  void on_pixel67_clicked();
  void on_pixel68_clicked();
  void on_pixel71_clicked();
  void on_pixel72_clicked();
  void on_pixel73_clicked();
  void on_pixel74_clicked();
  void on_pixel75_clicked();
  void on_pixel76_clicked();
  void on_pixel77_clicked();
  void on_pixel78_clicked();
  void on_pixel81_clicked();
  void on_pixel82_clicked();
  void on_pixel83_clicked();
  void on_pixel84_clicked();
  void on_pixel85_clicked();
  void on_pixel86_clicked();
  void on_pixel87_clicked();
  void on_pixel88_clicked();
  void on_pixel91_clicked();
  void on_pixel92_clicked();
  void on_pixel93_clicked();
  void on_pixel94_clicked();
  void on_pixel95_clicked();
  void on_pixel96_clicked();
  void on_pixel97_clicked();
  void on_pixel98_clicked();
  void on_pixel101_clicked();
  void on_pixel102_clicked();
  void on_pixel103_clicked();
  void on_pixel104_clicked();
  void on_pixel105_clicked();
  void on_pixel106_clicked();
  void on_pixel107_clicked();
  void on_pixel108_clicked();

  void on_pushButtonAprender_clicked();

  void on_pushButtonRodar_clicked();

  void on_comboBoxFonte2_currentIndexChanged(int index);

private:

  void PopulateTrainingSet(void);
  void PopulatePixelsVector(void);
  void UpdateDisplay(QVector<int> inputs);



  Ui::MainWindow *ui;

  QIcon m_black;
  QIcon m_white;

  QVector<int> m_inputs;
  QVector<QPushButton*> m_pixels;
  QVector<QVector<int>> m_fontA, m_fontB;
  QVector<QVector<QPair<QVector<int>, int>>> m_trainingSet;
  QVector<Neuronio*> m_neuronio;
  double m_tetha, m_alpha;
  int m_bias;
};

#endif // MAINWINDOW_H
