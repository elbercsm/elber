#include "neuronio.h"
#include <QtDebug>



Neuronio::Neuronio(double alpha, double bias, double tetha, QVector<double> peso) {
        m_alpha = alpha;
        m_bias = bias;
        m_tetha = tetha;
        m_peso = peso;
    }


double Neuronio::CalculateRede(QVector<int> entrada){
        double rede = 0;

        for(int i = 0; i < entrada.size(); i++){
            rede += entrada.at(i) * m_peso.at(i);
        }

        rede += m_bias;

        return rede;
    }

int Neuronio::CalculateOutput(QVector<int> entrada){
        double rede = CalculateRede(entrada);


        if (rede > m_tetha)
        {
            return 1;
        }
        else if (rede < (-1)*m_tetha)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }

