#ifndef NEURONIO_H
#define NEURONIO_H
#include <QVector>

class Neuronio
{

public:
    Neuronio(double alpha, double bias, double tetha, QVector<double> peso);
    double CalculateRede(QVector<int> entrada);
    int CalculateOutput(QVector<int> entrada);

private:
    double m_alpha, m_bias, m_tetha;
    QVector<double> m_peso;

};

#endif // NEURONIO_H
