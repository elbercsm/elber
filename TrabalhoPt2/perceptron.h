#ifndef PERCEPTRON_H
#define PERCEPTRON_H

#include <QVector>
#include <QMap>

class Perceptron
{
public:

    Perceptron();

    int GetNumOfEpochs(void);
    void UpdateParameters( QVector<QPair<QVector<int>,int>> trainingSet,
                          int bias,
                          double tetha,
                          double alpha);
    void InitWeights(void);
    double CalculateRede(QVector<int> inputs);
    int CalculateOutput(QVector<int> inputs);
    bool CheckStopCondition(QVector<double> oldWeights, double oldBias);
    QVector<double> CalculateWeights(void);
    int GetBias();
    int GetTetha();

private:
   double m_bias;
    int m_numOfInputs;
    int m_numOfEpochs;
    double m_tetha;
    double m_alpha;

    QVector<double> m_weights;
     QVector<QPair<QVector<int>, int>> m_trainingSet;

};

#endif // PERCEPTRON_H
