#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressDialog>

#include "ga.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_multiRunRadioButton_toggled(bool checked);

  void on_runPushButton_clicked();

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
