#include "ga.h"

Ga::Ga(int popSize, double crossoverRate, double mutationRate, int generations, bool elitism)
{
  m_popSize = popSize;
  m_crossoverRate = crossoverRate;
  m_mutationRate = mutationRate;
  m_generations = generations;
  m_elitism = elitism;

  m_population = QVector<double>();
  m_stringPopulation = QVector<QString>();
  m_fitness = QVector<double>();
  m_selected = QVector<double>();
  m_allX = QVector<double>();
  m_sumFitness = 0.0;
  m_bestIndividual = 0.0;


  // 10 - number of bits
  // 2^10 = 1024 possibilities of x
  // 512 - upper bound
  m_granularity = pow(2, 10);
  m_step = 512.0/m_granularity;

  // Getting all x
  GetAllX();

}

void Ga::GetAllX (void)
{
  m_allX = QVector<double>();

  for (int i = 0; i < m_granularity; i++)
  {
    m_allX.append(i*m_step);
  }

}


QString Ga::GetBin(int decimal)
{
  QString binary;
  binary.setNum(decimal, 2);
  int s = binary.size();

  // 10 - number of bits
  for(int i = 0; i < (10 - s); i++)
    binary.prepend('0');

  return binary;
}

double Ga::GetDecimal (QString binary)
{
  bool ok;
  int decimal = binary.toInt(&ok, 2);

  return decimal*m_step;

}

void Ga::GetInitialPopulation(void)
{
  QVector<int> sampled = QVector<int>();
  m_population = QVector<double>();

  for (int i = 0; i < m_popSize; i++)
  {
    int index = QRandomGenerator::global()->bounded(static_cast<int>(m_granularity));

    while (sampled.contains(index))
    {
      index = QRandomGenerator::global()->bounded(static_cast<int>(m_granularity));
    }

    m_population.append(m_allX.at(index));
    sampled.append(index);
  }

}

void Ga::Evaluate (void)
{
  m_fitness = QVector<double>();
  m_sumFitness = 0.0;

  for (int i = 0; i < m_popSize; i++)
  {
    // Inserir como a função de aptidão é calculada
    // ...
      double calcula;
      calcula = (-1)*abs(m_population.at(i)*sin(sqrt(m_population.at(i))));
      m_fitness.append(calcula);
      m_sumFitness +=calcula;

  }

  // Seleciona o melhor indivíduo para ser utilizado,
  // caso o elistimo esteja selecionado
  m_bestIndividual = m_population.at(m_fitness.indexOf(*std::min_element(m_fitness.constBegin(), m_fitness.constEnd())));
}

void Ga::Select (void)
{
    m_selected.clear();
  //Seleciona os indivíduos para a recombinação
  QVector<double> prob = QVector<double>();
  prob.append(m_fitness.at(0)/m_sumFitness);
 // double soma = 0.0;

  for ( int i = 1 ; i < m_fitness.size() ; i++){
     // soma += m_fitness.at(i);
           // prob.append(soma/m_sumFitness);
      //prob.append(soma/m_sumFitness);
      prob.append(prob.at(i-1)+(m_fitness.at(i)/m_sumFitness));
  }

  for ( int j = 0 ; j <m_population.size() ; j++){

    double index = QRandomGenerator::global()->bounded(1.0);

    for(int i = 0 ; i < prob.size() ; i++){
        if (index <= prob.at(i)){
            m_selected.append(m_population.at(i));
            break;
            }
        }
    }
}


void Ga::Cross(void)
{
    m_stringPopulation.clear();


  // Aplica a operação de cruzamento
    QVector<QPair<QString, QString>> casais = QVector<QPair<QString, QString>>();
    for (int i = 0; i < m_selected.size()/2; i++){

        int index = QRandomGenerator::global()->bounded(m_selected.size());
        int index2 = QRandomGenerator::global()->bounded(m_selected.size());

       // (GetBin(m_selected.at(index)/m_step));
        QString p1 = (GetBin (static_cast<int>((m_selected.at(index)/m_step))));
        QString p2 = (GetBin (static_cast<int>((m_selected.at(index2)/m_step))));

        casais.append(qMakePair(p1,p2));
    }


    for(int i = 0 ; i< casais.length();i++){
       QString pai = casais.at(i).first;
       QString mae = casais.at(i).second;

         int corte = QRandomGenerator::global()->bounded(0,mae.size()-1);
    //   int randPai = QRandomGenerator::global()->bounded(0,pai.size()-1);

    if(QRandomGenerator::global()->bounded(1.0)<=m_crossoverRate){
           QString filho1 = pai.mid(0, corte) + mae.mid(corte, pai.size());
           QString filho2 = mae.mid(0, corte) + pai.mid(corte, mae.size());
           m_stringPopulation.append(filho1);
           m_stringPopulation.append(filho2);
    }else{
           m_stringPopulation.append(pai);
           m_stringPopulation.append(mae);
       }


     //  qDebug()<<filho1;
     //  qDebug()<<filho2;
}

}

void Ga::Mutate (void)
{
  // Aplica a operação de mutação
    for( int i=0 ;i<m_stringPopulation.size();i++){
        if(QRandomGenerator::global()->bounded(1.0)<=m_mutationRate){
            int aux=QRandomGenerator::global()->bounded(m_stringPopulation.at(i).length());
            if(m_stringPopulation.at(i).at(aux)=="0"){
                m_stringPopulation[i].replace(aux, "1");
            }else{
                 m_stringPopulation[i].replace(aux, "0");
            }
        }
        m_population[i]=GetDecimal(m_stringPopulation.at(i));
    }

}


double Ga::RunGenerations(void)
{
  // Método de controle do Algoritmo Genético (ver fluxograma no Canvas).
       //inicia a populacao
    GetInitialPopulation();
  for(int i = 0;i < m_generations ;i++){
      Evaluate();
      Select();
      Cross();
      Mutate();
      if(m_elitism){
         int rand = QRandomGenerator::global()->bounded(0,m_popSize);
          m_population[rand]=m_bestIndividual;
      }

}
  Evaluate();

  return  m_bestIndividual;
}

