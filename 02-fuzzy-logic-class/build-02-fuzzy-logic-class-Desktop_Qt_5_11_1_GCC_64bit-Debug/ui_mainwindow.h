/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_5;
    QGridLayout *gridLayout;
    QLabel *lblMoney;
    QSpinBox *spnMoney;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout_2;
    QLabel *lblPeople;
    QSpinBox *spnPeople;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;
    QPushButton *btnCalc;
    QSpacerItem *verticalSpacer_3;
    QGridLayout *gridLayout_3;
    QLabel *lblrisk;
    QLabel *lblRiskText;
    QSpacerItem *verticalSpacer_4;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_5);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lblMoney = new QLabel(centralWidget);
        lblMoney->setObjectName(QStringLiteral("lblMoney"));

        gridLayout->addWidget(lblMoney, 0, 0, 1, 1);

        spnMoney = new QSpinBox(centralWidget);
        spnMoney->setObjectName(QStringLiteral("spnMoney"));

        gridLayout->addWidget(spnMoney, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 2, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        lblPeople = new QLabel(centralWidget);
        lblPeople->setObjectName(QStringLiteral("lblPeople"));

        gridLayout_2->addWidget(lblPeople, 0, 0, 1, 1);

        spnPeople = new QSpinBox(centralWidget);
        spnPeople->setObjectName(QStringLiteral("spnPeople"));

        gridLayout_2->addWidget(spnPeople, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 0, 2, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        btnCalc = new QPushButton(centralWidget);
        btnCalc->setObjectName(QStringLiteral("btnCalc"));

        verticalLayout->addWidget(btnCalc);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        lblrisk = new QLabel(centralWidget);
        lblrisk->setObjectName(QStringLiteral("lblrisk"));

        gridLayout_3->addWidget(lblrisk, 0, 0, 1, 1);

        lblRiskText = new QLabel(centralWidget);
        lblRiskText->setObjectName(QStringLiteral("lblRiskText"));

        gridLayout_3->addWidget(lblRiskText, 0, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_3);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "FuzztLogic", nullptr));
        lblMoney->setText(QApplication::translate("MainWindow", "Dinheiro (em milh\303\265es):", nullptr));
        lblPeople->setText(QApplication::translate("MainWindow", "Pessoas:", nullptr));
        btnCalc->setText(QApplication::translate("MainWindow", "Calcular", nullptr));
        lblrisk->setText(QApplication::translate("MainWindow", "Risco", nullptr));
        lblRiskText->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
