#ifndef RULER_H
#define RULER_H

#include <QVector>
#include <algorithm>

class Ruler{
public: Ruler();

    QVector<double> GetRulesResult (QVector<double> moneyMembership,
                                    QVector<double> peopleMembershio);
};

#endif // RULER_H
