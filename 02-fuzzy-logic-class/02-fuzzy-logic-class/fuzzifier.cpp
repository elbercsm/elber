#include "fuzzifier.h"
#include <QDebug>


Fuzzifier::Fuzzifier(){
}



    QVector<double> Fuzzifier::GetMoneyFuzzyfication(int money){
    QVector<double> moneyFuzzification;
    double little; //pouco
    double fair; //razoável
    double suitable; //adequado
    qDebug()<<"fuzzifier - Linha 15";

        if(money<= 30){
            little = 1.0;
            fair = 0.0;
            suitable = 0.0;
        }else if((money > 30)&(money <= 50)){
            little = (50.0 - money)/(50.0 - 30.0);
            fair = (money - 30.0)/(50.0 - 30.0);
            suitable = 0.0;
        }else if ((money > 50)&(money < 70)){
            little = 0.0;
            fair = (70.0 - money)/(70.0 - 50.0);
            suitable = (money - 50.0)/(70.0 - 50.0);

        }else if(money >= 70){
            little = 0.0;
            fair = 0.0;
            suitable = 1.0;
        }else{
            qDebug()<<"Error! Case not expected!";
        }

    moneyFuzzification.append(little);
    moneyFuzzification.append(fair);
    moneyFuzzification.append(suitable);
    qDebug()<<"fuzzifier - Linha 41";
    return moneyFuzzification;
        }

    QVector<double> Fuzzifier::GetPeopleFuzzyfication(int people){
    QVector<double> peopleFuzzification;
    double insufficient;   //insuficiente
    double satisfactory;  //satisfatorio

            if(people < 30){
                insufficient = 1.0;
                satisfactory = 0.0;
            }else if((people >= 30) & (people <= 70)){
                insufficient = (70.0 - people)/(70.0 - 30.0);
                satisfactory = (people - 30)/(70.0 - 30.0);
            }else if (people > 70){
                insufficient = 0.0;
                satisfactory = 1.0;
            }else{
                qDebug()<<"Error! Case not expected!";
            }
            peopleFuzzification.append(insufficient);
            peopleFuzzification.append(satisfactory);
            return peopleFuzzification;
        }




