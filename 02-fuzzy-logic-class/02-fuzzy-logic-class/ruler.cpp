#include "ruler.h"

Ruler::Ruler()
{

}


QVector<double> Ruler::GetRulesResult(QVector<double> moneyMembership,
                                      QVector<double> peopleMembership){

    QVector<double> rulesResult;
    // SE u_D-adequado e u_P-satistatorio ENTÃO u_R-baixo
    rulesResult.append(std::min( moneyMembership.at(2), peopleMembership.at(1) ));

    // SE u_D-rasoavel e u_P-satistatorio ENTÃO u_R-medio
    rulesResult.append(std::min( moneyMembership.at(1), peopleMembership.at(1) ));

    // SE u_D-pouco e u_P-satistatorio ENTÃO u_R-alto
    rulesResult.append(std::min( moneyMembership.at(0), peopleMembership.at(1) ));

    // SE u_D-pouco e u_P-insuficiente ENTÃO u_R-alto
    if(std::max(moneyMembership.at(0), peopleMembership.at(0) ) > rulesResult.at(2)){
    rulesResult[2] = std::max(moneyMembership.at(0), peopleMembership.at(0) );
    }
    return rulesResult;
}
